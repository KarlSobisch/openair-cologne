from .SOAISensor import SOAISensor

import logging
logger = logging.getLogger()


## Class which represents a Lanuv sensor
#
# This is kind of a dummy class since a Lanuv sensor does not need to treat different then a normal sensor
class SOAILanuvSensor(SOAISensor):

    ## Initializes one Lanuv sensor
    #
    # @param ID ID (feed) of the sensor
    # @param location Location of the sensor given as tuple (latitude, longitude)
    def __init__(self, ID, location):
        logger.debug(f"Set up Lanuv sensor with ID {ID} at location {location}")
        super().__init__("Lanuv", ID, location)

        self.active = True

    ## Since the data is already a calibrated NO2 value this is a dummy function
    #
    # @param dataDict Dictionary with the features {no2: x} (measurment corresponds to NO2 value)
    def fDataToNO2(self, dataDict):
        return dataDict["no2"]
