from .SOAISensor import SOAISensor
from .SOAIOpenAirCalibrationModel import SOAIOpenAirCalibrationModel

import os
import logging
import numpy as np

logger = logging.getLogger()


## Class which represents an OpenAirSensor
#
# This class differs from a normal sensor by using a calibration model to convert measurments to NO2.
class SOAIOpenAirSensor(SOAISensor):

    ## Initializes one OpenAir Cologne sensor
    #
    # @param ID ID (feed) of the sensor
    # @param location Location of the sensor given as tuple (latitude, longitude)
    def __init__(self, ID, location):
        logger.debug(f"Set up OpenAir Cologne Sensor with ID {ID} at location {location}")

        super().__init__("OpenAirCologne", ID, location)

        self.calibModel = None

    ## Set the calibration model
    #
    # @param pathToModel Path to model to use for calibration
    # @param pathToScaler Path to scaler which is used to scale the data before prediction
    def fSetCalibration(self, pathToModel, pathToScaler):
        logger.debug(f"Set up calibration model for OpenAir Cologne sensior with ID {self.ID} at location {self.location}")
        if os.path.isfile(pathToModel):
            self.calibModel = SOAIOpenAirCalibrationModel(pathToModel, pathToScaler)
        else:
            logger.warning(f"No calibration model found in path {pathToModel}. Set sensor as inactive.")
            self.fSetInactive()

    ## Converts one data sample to an NO2 value using the calibration model
    #
    # @param dataDict Dictionary with the features {r2: x, temp: y, hum: z} (in this case measurment corresponds to r2)
    def fDataToNO2(self, dataDict):
        if self.calibModel is None:
            logger.warning("No calibration model found. Is this sensor calibrated?")
            raise Exception("No calibration model found.")

        data = np.array([dataDict["hum"], dataDict["temp"], dataDict["r2"]])
        data = data.reshape(1, -1)
        no2 = self.calibModel.fPredict(data)

        return no2

    ## Returns a boolean whether a calibration model is set
    def fHasCalibration(self):
        if self.calibModel is not None:
            return True
        else:
            return False
