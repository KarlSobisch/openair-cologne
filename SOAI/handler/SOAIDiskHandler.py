from .SOAIDataHandler import SOAIDataHandler

import pandas as pd
import glob
import logging
import json

logger = logging.getLogger()


## Class which handles the loading of data from disk.
class SOAIDiskHandler(SOAIDataHandler):

    ## Constructor
    def __init__(self):
        pass

    ## Load data from OpenAir Cologne
    #
    # @param path Path to folder with files
    # @param selectValidData Boolean if only valid data shall be loaded
    # @returns Pandas data frame with measurment values depending on time
    def fGetOpenAir(self, path="./data/openair/", selectValidData=True):
        logger.debug(f"Load OpenAir Cologne data from {path}.")

        # Since the measurments are saved in multiple files, use a glob-string and wildcards in order to load the data
        filesOpenAir = glob.glob(path + "*.parquet")
        listOpenAir = []
        for filename in filesOpenAir:
            logger.debug(f"\t- Load {filename}")
            df = pd.read_parquet(filename)
            listOpenAir.append(df)

        # Sort the values depending on time
        dataOpenAir = pd.concat(listOpenAir)

        # Check the OpenAir data
        dataOpenAir = self._fCheckOpenAir(dataOpenAir, selectValidData)

        return dataOpenAir

    ## Load properties of OpenAir Cologn sensors
    #
    # @param pathToFile Path to file where sensor data is saved
    # @param selectValidData Boolean if only valid sensor data shall be loaded
    # @returns Pandas data frame with location of the sensors
    def fGetOpenAirSensors(self, pathToFile="./data/openair/sensors.json"):
        logger.debug(f"Load open air sensor properties from {pathToFile}.")

        # Open json data where properties of senesors are encoded
        with open(pathToFile) as f:
            data = json.load(f)

        # Create a data frame with location information about the sensors
        df = pd.DataFrame(columns=["feed", "lon", "lat"])
        for feed in data["features"]:
            lonCoordinate = feed["geometry"]["coordinates"][0]
            latCoordinate = feed["geometry"]["coordinates"][1]
            mqtt_id = feed["properties"]["mqtt_id"].split("-")[0]

            df = df.append({"feed": mqtt_id, "lon": lonCoordinate, "lat": latCoordinate}, ignore_index=True)

        return df

    ## Load data from Luftdaten.info
    #
    # @param path Path to folder with files
    # @param selectValidData Boolean if only valid data shall be loaded
    # @returns Pandas data frame with measurment values depending on time
    def fGetLanuv(self, path="./data/lanuv/", selectValidData=False):
        logger.debug(f"Load Lanuv data from {path}.")

        # Since the measurments are saved in multiple files, use a glob-string and wildcards in order to load the data
        filesLanuv = glob.glob(path + "*.parquet")
        listLanuv = []
        for filename in filesLanuv:
            logger.debug(f"\t- Load {filename}")
            df = pd.read_parquet(filename)
            listLanuv.append(df)

        # Concat data
        dataLanuv = pd.concat(listLanuv, ignore_index=True)

        # Check the Lanuv data
        dataLanuv = self._fCheckLanuv(dataLanuv, selectValidData)

        return dataLanuv

    ## Load properties of OpenAir Cologn sensors
    #
    # @param pathToFile Path to file where sensor data is saved
    # @returns Pandas data frame with location of the sensors
    def fGetLanuvSensors(self, pathToFile="./data/lanuv/sensors.csv"):
        logger.debug(f"Load lanuv sensor properties from {pathToFile}.")

        df = pd.read_csv(pathToFile, sep=";")

        return df
